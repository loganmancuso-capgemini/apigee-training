'''
	'aprog.py'
	Author/CopyRight: Mancuso, Logan
	Last Edit Date: 07-07-2019--00:24:42
'''

# Packages
import sys
import os

### File I/O functions

def read_in( in_file ):
	lines = ""
	with open(in_file) as text_file:
		lines = text_file.read().splitlines()
		#lines is an array of lines from the in_file
		text_file.close()
	return lines

def write_out( out_file, out_stream ):
	with open(out_file,"a+") as text_file:
		text_file.write(out_stream)
		text_file.close()

### main function 
def main(argv):
	in_file = argv[1]
	out_file = argv[2]
	log_file = argv[3]

	lines_read = read_in( in_file )
	write_out( log_file, "Beginning Computation\n" )
	
	# x is an element in the array of lines read from input file
	# call dothework script to begin processing from file
	from dothework import __init__
	__init__(lines_read,out_file,log_file)
	'''
		add code to dothework file and return it to the main
	'''
	write_out( log_file, "Ending Computation")


### main function call
if __name__ == "__main__":
	main(sys.argv) #execute main function

'''
	End 'aprog.py'
'''