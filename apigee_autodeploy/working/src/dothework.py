'''
	'dothework.py'
	Author/CopyRight: Mancuso, Logan
	Last Edit Date: 07-07-2019--00:24:42
'''

# Packages
import sys
import os

### File I/O function
def write_out( out_file, out_stream ):
	with open(out_file,"a+") as text_file:
		text_file.write(out_stream + "\n")
		text_file.close()

### initialize the computation by reading from the file
def __init__(input_array, out_file, log_file):
	# input array is an array comprised of each line from the input file
	for a_line in input_array:
		write_out( out_file,"Read This: " + a_line )

# compute
def compute():
	return 0

'''
	End 'dothework.py'
'''